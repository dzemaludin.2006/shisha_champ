"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var dotenv_1 = __importDefault(require("dotenv"));
var cors_1 = __importDefault(require("cors"));
var typeorm_1 = require("typeorm");
var UserRepository_1 = require("./Repo/UserRepository");
var TeamRepository_1 = require("./Repo/TeamRepository");
var database_1 = __importDefault(require("./config/database"));
require("reflect-metadata");
var user_1 = require("./entity/user");
var team_1 = require("./entity/team");
var ScoresRepositroy_1 = require("./Repo/ScoresRepositroy");
var UserandTeams_1 = require("./Repo/UserandTeams");
var user_team_1 = require("./entity/user_team");
var scores_1 = require("./entity/scores");
dotenv_1.default.config();
var app = (0, express_1.default)();
(0, database_1.default)();
app.use((0, cors_1.default)({ origin: "*", credentials: true }));
app.use(express_1.default.urlencoded({ extended: true }));
app.use(express_1.default.json());
//users api cals
//add users
app.post("/add-user", function (req, res, next) {
    try {
        var user_repo = (0, typeorm_1.getCustomRepository)(UserRepository_1.Userepository);
        user_repo.addUser({
            name: req.body.name,
            score: 0,
            aditonal_score: 0,
        });
        res.sendStatus(200);
    }
    catch (error) {
        next(error);
    }
});
app.post("/add-aditional-score", function (req, res, next) {
    try {
        var user_repo = (0, typeorm_1.getCustomRepository)(UserRepository_1.Userepository);
        var id = req.body.id;
        var additional_core = req.body.additional_core;
        user_repo
            .createQueryBuilder()
            .update(user_1.User)
            .set({
            additional_core: additional_core,
        })
            .where("id = :id", { id: id })
            .execute();
        res.sendStatus(200);
    }
    catch (error) {
        next(error);
    }
});
//get all of the users or some of them
app.get("/get-users", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user_repo, user_and_team_repo, users, sorted_users, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                user_repo = (0, typeorm_1.getCustomRepository)(UserRepository_1.Userepository);
                user_and_team_repo = (0, typeorm_1.getCustomRepository)(UserandTeams_1.UserAndTeamrepozitory);
                return [4 /*yield*/, user_repo.find()];
            case 1:
                users = _a.sent();
                sorted_users = users.sort(function (a, b) {
                    var full_score1 = a.additional_core + a.score;
                    var full_score2 = b.additional_core + b.score;
                    if (full_score1 < full_score2) {
                        return 1;
                    }
                    else if (full_score1 > full_score2) {
                        return -1;
                    }
                    else {
                        return 0;
                    }
                });
                res.json(sorted_users);
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                next(error_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
//deleting users
app.delete("/delete-user/", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user_repo, user_and_team_repo, id;
    return __generator(this, function (_a) {
        try {
            user_repo = (0, typeorm_1.getCustomRepository)(UserRepository_1.Userepository);
            user_and_team_repo = (0, typeorm_1.getCustomRepository)(UserandTeams_1.UserAndTeamrepozitory);
            id = req.body.id;
            user_repo
                .createQueryBuilder()
                .delete()
                .from(user_1.User)
                .where("id = :id", { id: id })
                .execute();
            user_and_team_repo
                .createQueryBuilder()
                .delete()
                .from(user_team_1.UserAndTeam)
                .where("user_id = :user_id", { user_id: id })
                .execute();
            res.sendStatus(200).send("user deleted");
        }
        catch (error) { }
        return [2 /*return*/];
    });
}); });
//get footbal teams
app.get("/get-teams", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user_repo, _a, _b, error_2;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                _c.trys.push([0, 2, , 3]);
                user_repo = (0, typeorm_1.getCustomRepository)(TeamRepository_1.Teamrepository)
                    .createQueryBuilder("team")
                    .orderBy("team.name", "ASC")
                    .getMany();
                _b = (_a = res).json;
                return [4 /*yield*/, user_repo];
            case 1:
                _b.apply(_a, [_c.sent()]);
                return [3 /*break*/, 3];
            case 2:
                error_2 = _c.sent();
                next(error_2);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/get-user-teams", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user_repo, user_and_team_repo_1, users, team_repo_1, new_users, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                user_repo = (0, typeorm_1.getCustomRepository)(UserRepository_1.Userepository);
                user_and_team_repo_1 = (0, typeorm_1.getCustomRepository)(UserandTeams_1.UserAndTeamrepozitory);
                return [4 /*yield*/, user_repo.find()];
            case 1:
                users = _a.sent();
                team_repo_1 = (0, typeorm_1.getCustomRepository)(TeamRepository_1.Teamrepository);
                return [4 /*yield*/, Promise.all(users.map(function (user) { return __awaiter(void 0, void 0, void 0, function () {
                        var new_user_teams, new_user_data;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, user_and_team_repo_1.find({
                                        user_id: user.id,
                                    })];
                                case 1:
                                    new_user_teams = _a.sent();
                                    return [4 /*yield*/, Promise.all(new_user_teams.map(function (team) { return __awaiter(void 0, void 0, void 0, function () {
                                            var team_name;
                                            var _a;
                                            return __generator(this, function (_b) {
                                                switch (_b.label) {
                                                    case 0: return [4 /*yield*/, team_repo_1.findOne({ id: team.team_id })];
                                                    case 1:
                                                        team_name = (_a = (_b.sent())) === null || _a === void 0 ? void 0 : _a.name;
                                                        return [2 /*return*/, team_name];
                                                }
                                            });
                                        }); }))];
                                case 2:
                                    new_user_data = _a.sent();
                                    return [2 /*return*/, {
                                            id: user.id,
                                            teams: new_user_data,
                                            name: user.name,
                                        }];
                            }
                        });
                    }); }))];
            case 2:
                new_users = _a.sent();
                res.json(new_users);
                return [3 /*break*/, 4];
            case 3:
                error_3 = _a.sent();
                next(error_3);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
// add footbal teams
app.post("/add-team", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var team_repo;
    return __generator(this, function (_a) {
        try {
            team_repo = (0, typeorm_1.getCustomRepository)(TeamRepository_1.Teamrepository);
            team_repo.addTeam(req.body.name);
            res.sendStatus(200);
        }
        catch (error) {
            next(error);
        }
        return [2 /*return*/];
    });
}); });
//get all of the teams
// //delete team
app.delete("/delete-team", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user_repo_1, user_and_team_repo, team_repo, id, teams, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                user_repo_1 = (0, typeorm_1.getCustomRepository)(TeamRepository_1.Teamrepository);
                user_and_team_repo = (0, typeorm_1.getCustomRepository)(UserandTeams_1.UserAndTeamrepozitory);
                team_repo = (0, typeorm_1.getCustomRepository)(TeamRepository_1.Teamrepository);
                id = req.body.id;
                return [4 /*yield*/, user_and_team_repo.findAllTeams(id)];
            case 1:
                teams = _a.sent();
                teams.forEach(function (team) {
                    user_repo_1
                        .createQueryBuilder()
                        .update(user_1.User)
                        .set({
                        score: function () { return "score - ".concat(team.score); },
                    })
                        .where("id = :id", { id: team.user_id })
                        .execute();
                });
                // za svaki item  u user oduzmi od scora
                user_and_team_repo
                    .createQueryBuilder()
                    .delete()
                    .from(user_team_1.UserAndTeam)
                    .where("team_id = :team_id", { team_id: id })
                    .execute();
                team_repo
                    .createQueryBuilder()
                    .delete()
                    .from(team_1.Team)
                    .where("id = :id", { id: id })
                    .execute();
                res.sendStatus(200);
                return [3 /*break*/, 3];
            case 2:
                error_4 = _a.sent();
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
//user & teams routes
app.post("/add-pair", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user_and_team_repo;
    return __generator(this, function (_a) {
        try {
            user_and_team_repo = (0, typeorm_1.getCustomRepository)(UserandTeams_1.UserAndTeamrepozitory);
            user_and_team_repo.addPair({
                user_id: req.body.user_id,
                team_id: req.body.team_id,
                score: 0,
            });
            res.sendStatus(200);
        }
        catch (error) {
            next(error);
        }
        return [2 /*return*/];
    });
}); });
app.get("/get-pairs", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user_and_team_repo, team_repo_2, user_repo_2, pairs, new_pair, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                user_and_team_repo = (0, typeorm_1.getCustomRepository)(UserandTeams_1.UserAndTeamrepozitory);
                team_repo_2 = (0, typeorm_1.getCustomRepository)(TeamRepository_1.Teamrepository);
                user_repo_2 = (0, typeorm_1.getCustomRepository)(UserRepository_1.Userepository);
                return [4 /*yield*/, user_and_team_repo
                        .createQueryBuilder("user_team")
                        .orderBy("user_team.user_id", "ASC")
                        .getMany()];
            case 1:
                pairs = _a.sent();
                return [4 /*yield*/, Promise.all(pairs.map(function (i) { return __awaiter(void 0, void 0, void 0, function () {
                        var team_name, user_name;
                        var _a, _b;
                        return __generator(this, function (_c) {
                            switch (_c.label) {
                                case 0: return [4 /*yield*/, team_repo_2.getTeamName(i.team_id)];
                                case 1:
                                    team_name = (_a = (_c.sent())) === null || _a === void 0 ? void 0 : _a.name;
                                    return [4 /*yield*/, user_repo_2.getUserData(i.user_id)];
                                case 2:
                                    user_name = (_b = (_c.sent())) === null || _b === void 0 ? void 0 : _b.name;
                                    return [2 /*return*/, {
                                            id: i.id,
                                            user_name: user_name,
                                            team_name: team_name,
                                            score: i.score,
                                        }];
                            }
                        });
                    }); }))];
            case 2:
                new_pair = _a.sent();
                res.json(new_pair);
                return [3 /*break*/, 4];
            case 3:
                error_5 = _a.sent();
                next(error_5);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
app.delete("/delete-pair", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user_repo, user_and_team_repo, id, score_1, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                user_repo = (0, typeorm_1.getCustomRepository)(TeamRepository_1.Teamrepository);
                user_and_team_repo = (0, typeorm_1.getCustomRepository)(UserandTeams_1.UserAndTeamrepozitory);
                id = req.body.id;
                return [4 /*yield*/, user_and_team_repo.findScore(id)];
            case 1:
                score_1 = _a.sent();
                //nadi svakog koji ima taj tim
                user_repo
                    .createQueryBuilder()
                    .update(user_1.User)
                    .set({
                    score: function () { return "score - ".concat(score_1); },
                })
                    .where("id = :id", { id: id })
                    .execute();
                user_and_team_repo
                    .createQueryBuilder()
                    .delete()
                    .from(user_team_1.UserAndTeam)
                    .where("id = :id", { id: id })
                    .execute();
                res.sendStatus(200);
                return [3 /*break*/, 3];
            case 2:
                error_6 = _a.sent();
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
//on what port is the app running
//matches routes
app.get("/get-score", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var team_repo_3, scores_repo, user_and_team_repo_2, user_repo_3, score, result, error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                team_repo_3 = (0, typeorm_1.getCustomRepository)(TeamRepository_1.Teamrepository);
                scores_repo = (0, typeorm_1.getCustomRepository)(ScoresRepositroy_1.Scoresrepository);
                user_and_team_repo_2 = (0, typeorm_1.getCustomRepository)(UserandTeams_1.UserAndTeamrepozitory);
                user_repo_3 = (0, typeorm_1.getCustomRepository)(UserRepository_1.Userepository);
                return [4 /*yield*/, scores_repo
                        .createQueryBuilder("scores")
                        .orderBy("scores.first_team_id", "ASC")
                        .getMany()];
            case 1:
                score = _a.sent();
                return [4 /*yield*/, Promise.all(score.map(function (i) { return __awaiter(void 0, void 0, void 0, function () {
                        var team1, user1, _a, _b, user2, _c, _d, team2;
                        var _e, _f, _g, _h;
                        return __generator(this, function (_j) {
                            switch (_j.label) {
                                case 0: return [4 /*yield*/, team_repo_3.getTeam(i.first_team_id)];
                                case 1:
                                    team1 = (_e = (_j.sent())) === null || _e === void 0 ? void 0 : _e.name;
                                    _b = (_a = user_repo_3).getUserData;
                                    return [4 /*yield*/, user_and_team_repo_2.findWinner(i.first_team_id)];
                                case 2: return [4 /*yield*/, _b.apply(_a, [_j.sent()])];
                                case 3:
                                    user1 = (_f = (_j.sent())) === null || _f === void 0 ? void 0 : _f.name;
                                    _d = (_c = user_repo_3).getUserData;
                                    return [4 /*yield*/, user_and_team_repo_2.findWinner(i.second_team_id)];
                                case 4: return [4 /*yield*/, _d.apply(_c, [_j.sent()])];
                                case 5:
                                    user2 = (_g = (_j.sent())) === null || _g === void 0 ? void 0 : _g.name;
                                    if (!user1) {
                                        user1 = "No user";
                                    }
                                    if (!user2) {
                                        user2 = "No user";
                                    }
                                    return [4 /*yield*/, team_repo_3.getTeam(i.second_team_id)];
                                case 6:
                                    team2 = (_h = (_j.sent())) === null || _h === void 0 ? void 0 : _h.name;
                                    return [2 /*return*/, {
                                            id: i.id,
                                            first_team_name: team1,
                                            first_team_score: i.first_team_score,
                                            first_team_user: user1,
                                            second_team_score: i.second_team_score,
                                            second_team_name: team2,
                                            second_team_user: user2,
                                        }];
                            }
                        });
                    }); }))];
            case 2:
                result = _a.sent();
                res.json(result);
                return [3 /*break*/, 4];
            case 3:
                error_7 = _a.sent();
                next(error_7);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
app.post("/add-score", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var scores_repo, user_and_team_repo_3, user_repo_4, first_score, second_score, updateUser;
    return __generator(this, function (_a) {
        try {
            scores_repo = (0, typeorm_1.getCustomRepository)(ScoresRepositroy_1.Scoresrepository);
            user_and_team_repo_3 = (0, typeorm_1.getCustomRepository)(UserandTeams_1.UserAndTeamrepozitory);
            user_repo_4 = (0, typeorm_1.getCustomRepository)(UserRepository_1.Userepository);
            first_score = req.body.first_team_score;
            second_score = req.body.second_team_score;
            scores_repo.addScore({
                first_team_id: req.body.first_team_id,
                second_team_id: req.body.second_team_id,
                first_team_score: first_score,
                second_team_score: second_score,
            });
            updateUser = function (id) { return __awaiter(void 0, void 0, void 0, function () {
                var user_id, score;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, user_and_team_repo_3.findWinner(id)];
                        case 1:
                            user_id = _a.sent();
                            score = 1;
                            user_and_team_repo_3
                                .createQueryBuilder()
                                .update(user_team_1.UserAndTeam)
                                .set({
                                score: function () { return "score + ".concat(score); },
                            })
                                .where("team_id = :team_id", { team_id: id })
                                .execute();
                            user_repo_4
                                .createQueryBuilder()
                                .update(user_1.User)
                                .set({
                                score: function () { return "score + ".concat(score); },
                            })
                                .where("id = :id", { id: user_id })
                                .execute();
                            return [2 /*return*/];
                    }
                });
            }); };
            if (first_score > second_score) {
                updateUser(req.body.first_team_id);
            }
            else {
                updateUser(req.body.second_team_id);
            }
            res.sendStatus(200);
        }
        catch (error) {
            next(error);
        }
        return [2 /*return*/];
    });
}); });
app.delete("/delete-score", function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var scores_repo, id;
    return __generator(this, function (_a) {
        try {
            scores_repo = (0, typeorm_1.getCustomRepository)(ScoresRepositroy_1.Scoresrepository);
            id = req.body.id;
            scores_repo
                .createQueryBuilder()
                .delete()
                .from(scores_1.Scores)
                .where("id = :id", { id: id })
                .execute();
            res.sendStatus(200);
        }
        catch (error) { }
        return [2 /*return*/];
    });
}); });
//on what port is the app running
app.listen(5199);
