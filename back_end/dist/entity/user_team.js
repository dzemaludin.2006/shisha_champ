"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserAndTeam = void 0;
var typeorm_1 = require("typeorm");
var UserAndTeam = /** @class */ (function () {
    function UserAndTeam(user_id, team_id, score) {
        this.user_id = user_id;
        this.team_id = team_id;
        this.score = score;
    }
    __decorate([
        (0, typeorm_1.PrimaryGeneratedColumn)(),
        __metadata("design:type", Number)
    ], UserAndTeam.prototype, "id", void 0);
    __decorate([
        (0, typeorm_1.Column)(),
        __metadata("design:type", Number)
    ], UserAndTeam.prototype, "user_id", void 0);
    __decorate([
        (0, typeorm_1.Column)(),
        __metadata("design:type", Number)
    ], UserAndTeam.prototype, "team_id", void 0);
    __decorate([
        (0, typeorm_1.Column)(),
        __metadata("design:type", Number)
    ], UserAndTeam.prototype, "score", void 0);
    UserAndTeam = __decorate([
        (0, typeorm_1.Entity)({ name: "user_team", synchronize: false }),
        __metadata("design:paramtypes", [Number, Number, Number])
    ], UserAndTeam);
    return UserAndTeam;
}());
exports.UserAndTeam = UserAndTeam;
